package main

import (
	cfg "trial-mito/config/env"
	_ "github.com/lib/pq"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"fmt"
	"database/sql"
	contactRepo "trial-mito/contact/repository"
	adminRepo"trial-mito/admin/repository"
	contactHttp "trial-mito/contact/delivery/contact_nw"
	adminHttp "trial-mito/admin/delivery/admin_nw"
	contactUcase "trial-mito/contact/usecase"
	adminUcase "trial-mito/admin/usecase"
	"github.com/sirupsen/logrus"
)

var config cfg.Config

//Viper config init
func init() {
	config = cfg.NewViperConfig()

	if config.GetBool(`debug`) {
		fmt.Println("Service RUN on DEBUG mode")
	}
}

//Initialize logger and sets levels
func loggerInit() *logrus.Logger{
	log := logrus.New()
	switch config.GetString("loggerlevel"){
	case  "info":
		log.SetLevel(logrus.InfoLevel)
		fmt.Println("Logrus logger set: InfoLevel")
	case "warning":
		log.SetLevel(logrus.WarnLevel)
		fmt.Println("Logrus logger set: WarnLevel")
	default :
		log.SetLevel(logrus.DebugLevel)
		fmt.Println("Logrus logger set: DebugLevel")
	}
	return log
}

//Set and check database connection, set echo webframework,
// and requests with CORS, init logger for the app,
//and starts server.
func main() {
	appSecret:= config.GetString("appsecret")
	dbHost := config.GetString(`database.host`)
	dbPort := config.GetString(`database.port`)
	dbUser := config.GetString(`database.user`)
	dbPass := config.GetString(`database.pass`)
	dbName := config.GetString(`database.name`)
	connection := fmt.Sprintf("host=%s port=%s user=%s "+
		"password=%s dbname=%s sslmode=disable",
		dbHost,
		dbPort,
		dbUser,
		dbPass,
		dbName)
	dbConn, err := sql.Open(`postgres`, connection)
	if err != nil && config.GetBool("debug") {
		fmt.Println(err)
	}
	defer dbConn.Close()
	e := echo.New()
	//Cors have default settings(allowed origins: "*")
	e.Use(middleware.CORS())
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	jwtMdlwr :=  middleware.JWTWithConfig(middleware.JWTConfig{
		SigningKey: []byte(appSecret),
	})

	log := loggerInit()

	cr := contactRepo.NewPostgresContactRepository(dbConn, log)
	cu := contactUcase.NewContactUsecase(cr, log)
	contactHttp.NewContactHttpHandler(e, cu, log)

	ar := adminRepo.NewPostgresAdminRepository(dbConn, log)
	au := adminUcase.NewAdminUsecase(ar, log, appSecret)
	adminHttp.NewAdminHttpHandler(e,au, log, jwtMdlwr)
	e.Start(config.GetString("server.address"))
}
