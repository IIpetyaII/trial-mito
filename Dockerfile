FROM golang:1.8

WORKDIR /go/src/trial-mito
COPY . .
EXPOSE 8080
RUN go get -d -v ./...
RUN go install -v ./...

CMD ["trial-mito"]