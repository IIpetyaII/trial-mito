CREATE SEQUENCE contacts_id_seq;

CREATE TABLE contacts
(
    id integer NOT NULL DEFAULT nextval('contacts_id_seq'),
    name text NOT NULL,
    email text NOT NULL,
    phone text NOT NULL,
    CONSTRAINT contacts_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);

ALTER SEQUENCE contacts_id_seq
OWNED BY contacts.id;

CREATE SEQUENCE administrators_id_seq;

CREATE TABLE administrators
(
    id integer NOT NULL DEFAULT nextval('administrators_id_seq'),
    name text NOT NULL,
    password text NOT NULL,
    CONSTRAINT administrators_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);

ALTER SEQUENCE administrators_id_seq
OWNED BY administrators.id;

insert into administrators (name, password) values ('Developer', 'Mypassword123');


--My dummies generated by https://mockaroo.com/

insert into contacts (name, email, phone) values ('Lorelei Spleving', 'lspleving0@exblog.jp', '3702148284');
insert into contacts (name, email, phone) values ('Carmina Potter', 'cpotter1@home.pl', '9766817678');
insert into contacts (name, email, phone) values ('Abbey Bruckman', 'abruckman2@w3.org', '1901043948');
insert into contacts (name, email, phone) values ('Alexandre Guise', 'aguise3@smugmug.com', '9812780470');
insert into contacts (name, email, phone) values ('Kellie Godwyn', 'kgodwyn4@dot.gov', '4292494796');
insert into contacts (name, email, phone) values ('Damaris Deluce', 'ddeluce5@sun.com', '5126513520');
insert into contacts (name, email, phone) values ('Caleb Van der Spohr', 'cvan6@msu.edu', '4925736162');
insert into contacts (name, email, phone) values ('Lyndel Vigneron', 'lvigneron7@edublogs.org', '9556003498');
insert into contacts (name, email, phone) values ('Mariette Kuzemka', 'mkuzemka8@hubpages.com', '7918850109');
insert into contacts (name, email, phone) values ('Garrek Gilmore', 'ggilmore9@aol.com', '1649613575');
insert into contacts (name, email, phone) values ('Cecelia Lewcock', 'clewcocka@hhs.gov', '5117802154');
insert into contacts (name, email, phone) values ('Nolie Franklen', 'nfranklenb@skype.com', '9141202932');
insert into contacts (name, email, phone) values ('Harold Sleany', 'hsleanyc@marriott.com', '6538658131');
insert into contacts (name, email, phone) values ('Garik Bankhurst', 'gbankhurstd@newsvine.com', '4229792196');
insert into contacts (name, email, phone) values ('Geraldine Dolman', 'gdolmane@howstuffworks.com', '8482721992');
insert into contacts (name, email, phone) values ('Ben Duck', 'bduckf@unesco.org', '7118081047');
insert into contacts (name, email, phone) values ('Imojean Kneeland', 'ikneelandg@businessweek.com', '8045588683');
insert into contacts (name, email, phone) values ('Carolyne Longfoot', 'clongfooth@163.com', '8531204680');
insert into contacts (name, email, phone) values ('Josephine Blackeby', 'jblackebyi@nymag.com', '5457608953');
insert into contacts (name, email, phone) values ('Donal Howick', 'dhowickj@wisc.edu', '5274581971');
insert into contacts (name, email, phone) values ('Daron Lemmens', 'dlemmensk@rambler.ru', '3392351340');
insert into contacts (name, email, phone) values ('Manon Geertsen', 'mgeertsenl@hao123.com', '3665060110');
insert into contacts (name, email, phone) values ('Thorny Phare', 'tpharem@amazon.co.uk', '2506026699');
insert into contacts (name, email, phone) values ('Harriett Willimont', 'hwillimontn@cyberchimps.com', '5106685395');
insert into contacts (name, email, phone) values ('Carlyle Upcraft', 'cupcrafto@wired.com', '7787519496');
insert into contacts (name, email, phone) values ('Isadore Wotton', 'iwottonp@example.com', '8998702629');
insert into contacts (name, email, phone) values ('Bernarr Elintune', 'belintuneq@apple.com', '8823809525');
insert into contacts (name, email, phone) values ('Paulie Weben', 'pwebenr@statcounter.com', '8652800916');
insert into contacts (name, email, phone) values ('Delinda Mains', 'dmainss@tumblr.com', '2912743734');
insert into contacts (name, email, phone) values ('Persis Shorten', 'pshortent@illinois.edu', '8181103749');
insert into contacts (name, email, phone) values ('Teresa Swadon', 'tswadonu@cloudflare.com', '3715398524');
insert into contacts (name, email, phone) values ('Brandice Le Lievre', 'blev@google.nl', '7018185815');
insert into contacts (name, email, phone) values ('Kym Izaac', 'kizaacw@time.com', '2934600008');
insert into contacts (name, email, phone) values ('Chrotoem Canwell', 'ccanwellx@usatoday.com', '1587999902');
insert into contacts (name, email, phone) values ('Abbye Macro', 'amacroy@goo.ne.jp', '9608772512');
insert into contacts (name, email, phone) values ('Connor Lafaye', 'clafayez@nymag.com', '3144168778');
insert into contacts (name, email, phone) values ('Cesya Cobbold', 'ccobbold10@imgur.com', '8219519292');
insert into contacts (name, email, phone) values ('Rodney Longmead', 'rlongmead11@statcounter.com', '8518022734');
insert into contacts (name, email, phone) values ('Henryetta Razzell', 'hrazzell12@msn.com', '5641055793');
insert into contacts (name, email, phone) values ('Florry Kitter', 'fkitter13@accuweather.com', '7689933464');
insert into contacts (name, email, phone) values ('Luciano Reeday', 'lreeday14@xrea.com', '8568453369');
insert into contacts (name, email, phone) values ('Mehetabel Pally', 'mpally15@ebay.co.uk', '8868438572');
insert into contacts (name, email, phone) values ('Berton Biggs', 'bbiggs16@purevolume.com', '9242083221');
insert into contacts (name, email, phone) values ('Evyn Alyukin', 'ealyukin17@sogou.com', '2291620588');
insert into contacts (name, email, phone) values ('Jada Gentery', 'jgentery18@creativecommons.org', '6189684615');
insert into contacts (name, email, phone) values ('Jacynth Stairs', 'jstairs19@apple.com', '7997510634');
insert into contacts (name, email, phone) values ('Rhianon Lethardy', 'rlethardy1a@elegantthemes.com', '4877098189');
insert into contacts (name, email, phone) values ('Cyb Shelbourne', 'cshelbourne1b@slashdot.org', '8476893140');
insert into contacts (name, email, phone) values ('Meaghan Hollows', 'mhollows1c@cam.ac.uk', '7205711644');
insert into contacts (name, email, phone) values ('Amabelle Tice', 'atice1d@elegantthemes.com', '9999812948');
insert into contacts (name, email, phone) values ('Clementina Agglione', 'cagglione1e@paypal.com', '6486555327');
insert into contacts (name, email, phone) values ('Orella Ville', 'oville1f@jimdo.com', '7079433973');
insert into contacts (name, email, phone) values ('Katya Garret', 'kgarret1g@shutterfly.com', '6724288061');
insert into contacts (name, email, phone) values ('Hunter Woolf', 'hwoolf1h@virginia.edu', '7375181747');
insert into contacts (name, email, phone) values ('Kyle Whenman', 'kwhenman1i@hubpages.com', '9527794386');
insert into contacts (name, email, phone) values ('Far Shalloe', 'fshalloe1j@oaic.gov.au', '3275235643');
insert into contacts (name, email, phone) values ('Justin Quene', 'jquene1k@marriott.com', '4973494928');
insert into contacts (name, email, phone) values ('Caterina Mardlin', 'cmardlin1l@ebay.com', '5949797391');
insert into contacts (name, email, phone) values ('Averil Causby', 'acausby1m@i2i.jp', '3834811057');
insert into contacts (name, email, phone) values ('Townie Juszczak', 'tjuszczak1n@ustream.tv', '1244272805');
insert into contacts (name, email, phone) values ('Sheff McArdell', 'smcardell1o@earthlink.net', '8623122629');
insert into contacts (name, email, phone) values ('Felice Bodle', 'fbodle1p@dmoz.org', '8109272580');
insert into contacts (name, email, phone) values ('Blondelle Altamirano', 'baltamirano1q@state.gov', '9012125956');
insert into contacts (name, email, phone) values ('Dione Brandassi', 'dbrandassi1r@walmart.com', '9287933685');
insert into contacts (name, email, phone) values ('Nara Keel', 'nkeel1s@seattletimes.com', '2513746803');
insert into contacts (name, email, phone) values ('Garrick Balaizot', 'gbalaizot1t@ed.gov', '7753534033');
insert into contacts (name, email, phone) values ('Klarika Simister', 'ksimister1u@vimeo.com', '9404098729');
insert into contacts (name, email, phone) values ('Eydie Winmill', 'ewinmill1v@github.com', '8688502757');
insert into contacts (name, email, phone) values ('Philippa Starr', 'pstarr1w@dailymotion.com', '2394043141');
insert into contacts (name, email, phone) values ('Betteann Goard', 'bgoard1x@apple.com', '8791456181');
insert into contacts (name, email, phone) values ('Emelen Peddie', 'epeddie1y@theglobeandmail.com', '5038411636');
insert into contacts (name, email, phone) values ('Jennette Barthot', 'jbarthot1z@va.gov', '4576213438');
insert into contacts (name, email, phone) values ('Gene Byllam', 'gbyllam20@biglobe.ne.jp', '7279915334');
insert into contacts (name, email, phone) values ('Bianca Mazillius', 'bmazillius21@amazon.co.jp', '2473966882');
insert into contacts (name, email, phone) values ('Timoteo Philippault', 'tphilippault22@ucsd.edu', '2022125151');
insert into contacts (name, email, phone) values ('Emilie Santorini', 'esantorini23@rediff.com', '9869836459');
insert into contacts (name, email, phone) values ('Belle Wraxall', 'bwraxall24@skyrock.com', '8175030335');
insert into contacts (name, email, phone) values ('Dido Laphorn', 'dlaphorn25@dyndns.org', '2913488963');
insert into contacts (name, email, phone) values ('Maisey Demetr', 'mdemetr26@taobao.com', '5003671580');
insert into contacts (name, email, phone) values ('Malinde Blayney', 'mblayney27@bbc.co.uk', '9522735916');
insert into contacts (name, email, phone) values ('Elfrida O''Ruane', 'eoruane28@booking.com', '8911340701');
insert into contacts (name, email, phone) values ('Dallas Keeltagh', 'dkeeltagh29@dropbox.com', '2454730778');
insert into contacts (name, email, phone) values ('Marena Bosward', 'mbosward2a@privacy.gov.au', '1861765127');
insert into contacts (name, email, phone) values ('Dale Persey', 'dpersey2b@mlb.com', '2239199921');
insert into contacts (name, email, phone) values ('Annecorinne Stow', 'astow2c@netlog.com', '4749010798');
insert into contacts (name, email, phone) values ('Matthias Malamore', 'mmalamore2d@tripadvisor.com', '5257493339');
insert into contacts (name, email, phone) values ('Othella Barock', 'obarock2e@ted.com', '2835188480');
insert into contacts (name, email, phone) values ('Clem Scotchbrook', 'cscotchbrook2f@jalbum.net', '5022587471');
insert into contacts (name, email, phone) values ('Margaretha Pointin', 'mpointin2g@fema.gov', '1626693349');
insert into contacts (name, email, phone) values ('Dosi Szymanowski', 'dszymanowski2h@gmpg.org', '3665834806');
insert into contacts (name, email, phone) values ('Karlik De-Ville', 'kdeville2i@sciencedaily.com', '6535854865');
insert into contacts (name, email, phone) values ('Ronnie Learmouth', 'rlearmouth2j@tripod.com', '9222185657');
insert into contacts (name, email, phone) values ('Sibeal Verlander', 'sverlander2k@wikia.com', '7865263979');
insert into contacts (name, email, phone) values ('Dorella Giriardelli', 'dgiriardelli2l@ning.com', '5364460001');
insert into contacts (name, email, phone) values ('Hamlen Blackader', 'hblackader2m@drupal.org', '8541151768');
insert into contacts (name, email, phone) values ('Wade Hanne', 'whanne2n@scribd.com', '8159272987');
insert into contacts (name, email, phone) values ('Nadine Fulham', 'nfulham2o@phoca.cz', '2475730472');
insert into contacts (name, email, phone) values ('Ortensia Mullard', 'omullard2p@lulu.com', '7345285869');
insert into contacts (name, email, phone) values ('Lesly Tolumello', 'ltolumello2q@oracle.com', '1555907695');
insert into contacts (name, email, phone) values ('Gino Fasse', 'gfasse2r@aol.com', '7661407087');


