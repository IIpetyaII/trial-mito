# Trial-Mito

Trial project

## Getting Started

At first you need golang 1.8+ on your machine and a
running Postgresql database listening on localhost:5432
(you can change the port number in config.json)

You can setting the app with the `./config.json` 
file.
 
In the project folder, run: `go build`

then: `./main`                         

App is listening on localhost:8080

## Running the tests
Now the app's code coverage is about 65-70%.
I used mocks, stubs and fake loads for my unit tests.
 
Automated tests are placed near the target .go files.
You can run them from their folders by `go test -v` command. 

## Built With

#####Echo Webframework

I tried more frameworks. My first choice was
Negroni, then tried Gin too. But Echo have a really 
friendly documentation and performs well in comparisons.

- https://go.libhunt.com/compare-echo-vs-negroni - More popular than
Negroni.

- https://github.com/labstack/echo - ...and better in performance than Gin(Check the graph! Fewer is better)

#####Package manager
The package manager of the project is Dep. - https://github.com/golang/dep

#####Database
The app can communicates with Postgresql database. 

#####SQL Queries

trial-mito/config/sql/init.sql

It produces not only the tables, and the Developer record in the
"admin" table, but 100 dummy records for the "contacts" table too.
If you don't need dummies, just comment them.
 
##Routes

- POST	/contact/:id	Edit a given person’s data
    - possible field examples:  
    
                "name": "Jhon Doe",
                "email": "email@email.com",
                "phone": "+0000000000"
                 
- GET	/contacts	List all people with their data
    - addtional options: cursor and num params
        - example request: localhost:8080/contacts?cursor=4&num=7
        If you only wants a portion of the whole contacts table, can use them.
        "num" defines the number of contact records
        "cursor" defines the id(not included) where the query starts
        The app sets an X-Cursor custom field in the Header of the response
        to the request for the next chunk.
- GET	/contact/:id	Get the given person with their data
- PUT	/contact	Add a person to the list with name, email and phone number
- DELETE	/contact/:id	Remove the selected person
- POST	/login	Log in with our test user Developer. It sends back a valid JWT token.
    - request Body payload example: 
          `                "name": "Developer",
                           "password": "Mypassword123"`                           
- GET	/login	Gives back the name of our developer
    - requires the valid token in Authorization header. 
    (In the test Frontend project, just put the valid token in the localstorage,
    with key: "token" and value: "the token itself")
  
  #### Docker - HALF SOLUTION
  
  The image is builded correctly, but it can't communicates with database.
  
  The problem is(I think) the app wants to find the localhost in its own environment,
  but I can't set the image correctly to find it's way to the DB on the root machine.
  
  The project have Dockerfile so you can build a docker image from it.
  - `sudo docker image build -t myapp .`  ("myapp" is only a name for example)
  
  The image can starts with this command, and listening for client requests correctly.
  -  `sudo docker container run -p 8080:8080 --rm myapp`
  
  But can't connect to the database. YET!
  
##Future Improvements

- More tests(90+% code coverage)
- Making interfaces for third party package services
- Security improvements
- Swagger
- Correct Dockerization

## Author
 Péter Szarka 

## Acknowledgments

I wanted to build a fully decoupled system, where every part
communicates through interfaces. Now the app
is fully manageable though the main.go via constructors. 
Implementations of new logics are easy to attach to this system 
and and mocking, and testing is so simple.
 
Go is absolutely a new language for me. 
I see its learning curve is shortly enough, and have a 
small but fantastically active community behind it.
The language itself is absolutely logic, the syntax is readable.
After this project I choosed it as my favourite backend language.
I hope I can use my new experiences on real projects. I really 
enjoy these ten days. I drink lot of coffee, not sleep too much, 
but worth it. I like this code and I'll don't stop to write it 
at this point. So, thanks for this few days!
  

