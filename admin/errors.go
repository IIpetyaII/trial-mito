package admin

import "errors"

//custom error messages
var (
	NOT_FOUND_ERROR       = errors.New("Your requested Item is not found")
)
