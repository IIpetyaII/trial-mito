package usecase

import (
	repo "trial-mito/admin/repository"
	"github.com/dgrijalva/jwt-go"
	"trial-mito/admin"
	"strings"
	"github.com/mitchellh/mapstructure"
	"github.com/sirupsen/logrus"
)

//AdminUsecase interface defines
//the behavior a usecase implementation.
type AdminUsecase interface {
	GetTokenByNameAndPass(name string, pass string) (*JwtToken, error)
	GetNameByToken(token string) (*admin.Admin, error)
	CreateToken(nm string, ps string) (*JwtToken, error)
}

//adminUsecase struct defines the usecase by its
//repository and logger instance and a key.
type adminUsecase struct {
	adminRepos repo.AdminRepository
	LOG *logrus.Logger
	Key string
}

//JwtToken wraps a token string for correct json format.
type JwtToken struct {
	Token string `json:"token"`
}

//NewAdminUsecase() returns a contactUsecase instance
//with its repository and logger instances.
func NewAdminUsecase(adminRepos repo.AdminRepository, LOG *logrus.Logger, Key string,) AdminUsecase {
	return &adminUsecase{
		adminRepos,
		LOG,
		Key}
}

//GetTokenByNameAndPass() checks for an admin in the database by
//its name and password. If it exists create a token and returns it.
func (a *adminUsecase) GetTokenByNameAndPass(nm string, ps string) (*JwtToken, error) {
	a.LOG.Debug("adminUsecase::GetTokenByNameAndPass()")
	ad, err := a.adminRepos.GetByNameAndPass(nm, ps)
	if err != nil {
		return nil, err
	}
	t, err := a.CreateToken(ad.Name, ad.Password)
	if err != nil {
		return nil, err
	}
	return t, nil
}

//GetNameByToken() first parse the received token string
//then decodes it with the proper decryption method using the secret key.
//Checks if the token valid(again) and if yes, makes an admin model by its
//claims. After that returns a new admin model only with the name field filled.
func (a *adminUsecase) GetNameByToken(t string) (*admin.Admin, error) {
	a.LOG.Debug("adminUsecase::GetNameByToken()")
	bt := strings.Split(t, " ")
	token, err := jwt.Parse(bt[1], func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, jwt.NewValidationError("Error while token parsing", 1)
		}
		return []byte(a.Key), nil
	})
	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		tad := new(admin.Admin)
		mapstructure.Decode(claims, &tad)
		ad := new(admin.Admin)
		ad.Name = tad.Name
		return ad, nil
	} else {
		a.LOG.Warning("Possible trying with an invalid token!")
		return nil, err
	}
}

//createToken() fill a token payload with name and password then makes the
//encryption using the apps secret key. Returns a newly created token wrapped
//JwtToken struct.
func (a *adminUsecase) CreateToken(nm string, ps string) (*JwtToken, error) {
	a.LOG.Debug("adminUsecase::GetNameByToken()")
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"name":     nm,
		"password": ps,
	})
	tokenString, err := token.SignedString([]byte(a.Key))
	if err != nil {
		return nil, err
	}
	return &JwtToken{Token: tokenString}, nil
}
