package usecase_test

import (
	"testing"
	"trial-mito/admin/repository/mocks"
	"github.com/stretchr/testify/mock"
	models"trial-mito/admin"
	uc "trial-mito/admin/usecase"
	"github.com/stretchr/testify/assert"
	"github.com/sirupsen/logrus"
)
func TestAdminUsecase_GetTokenByNameAndPass(t *testing.T) {
	mockAdminRepo :=  new(mocks.AdminRepository)
	mockAdmin := models.Admin{
		Name: "Emperor",
		Password: "mucus123",
	}
	mockToken := uc.JwtToken{
		Token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiRW1wZXJvciIsInBhc3N3b3JkIjoibXVjdXMxMjMifQ.rJCunzIlmgw7sbmyFgi8bzLNqJAh7kZqdbcTTUfNUlc",
	}

	mockAdminRepo.On("GetByNameAndPass", mock.AnythingOfType("string"), mock.AnythingOfType("string")).Return(&mockAdmin, nil)
	defer mockAdminRepo.AssertCalled(t,"GetByNameAndPass",mock.AnythingOfType("string"),mock.AnythingOfType("string"))

	log := logrus.New()
	key := "testkey"
	u := uc.NewAdminUsecase(mockAdminRepo, log, key)
	tk, err := u.GetTokenByNameAndPass(mockAdmin.Name,mockAdmin.Password)

	assert.NoError(t,err)
	assert.NotNil(t,tk)
	assert.Equal(t,tk, &mockToken)
}

func TestAdminUsecase_GetNameByToken(t *testing.T) {
	mockAdminRepo :=  new(mocks.AdminRepository)
	mockAdmin := models.Admin{
		Name: "Emperor",
	}
	tokenS := "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiRW1wZXJvciIsInBhc3N3b3JkIjoibXVjdXMxMjMifQ.rJCunzIlmgw7sbmyFgi8bzLNqJAh7kZqdbcTTUfNUlc"

	log := logrus.New()
	key := "testkey"
	u := uc.NewAdminUsecase(mockAdminRepo, log, key)

	ad, err := u.GetNameByToken(tokenS)
	assert.NoError(t,err)
	assert.NotNil(t,ad)
	assert.Equal(t,&mockAdmin,ad)
}

func TestAdminUsecase_CreateToken(t *testing.T) {
	mockAdminRepo :=  new(mocks.AdminRepository)
	mockAdmin := models.Admin{
		Name: "Emperor",
		Password: "mucus123",
	}

	mockToken := uc.JwtToken{
		Token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiRW1wZXJvciIsInBhc3N3b3JkIjoibXVjdXMxMjMifQ.rJCunzIlmgw7sbmyFgi8bzLNqJAh7kZqdbcTTUfNUlc",
	}
	log := logrus.New()
	key := "testkey"
	u := uc.NewAdminUsecase(mockAdminRepo, log, key)
	jwt, err := u.CreateToken(mockAdmin.Name, mockAdmin.Password)
	assert.NoError(t,err)
	assert.NotNil(t,jwt)
	assert.Equal(t,&mockToken,jwt)

}