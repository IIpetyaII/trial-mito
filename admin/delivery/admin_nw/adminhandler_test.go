package admin_nw_test

import (
	"testing"
	models "trial-mito/admin"
	"trial-mito/admin/usecase/mocks"
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"trial-mito/admin/usecase"
	"github.com/labstack/echo"
	"net/http"
	"strings"
	"net/http/httptest"
	httpAdmin "trial-mito/admin/delivery/admin_nw"
	"github.com/sirupsen/logrus"
)

func TestHttpAdminHandler_GetTokenByNameAndPass(t *testing.T) {
	mockAdmin := models.Admin{
		Name:     "Emperor",
		Password: "mucus123",
	}

	tempMockAdmin := mockAdmin
	tempMockAdmin.ID = 0

	mockUcase := new(mocks.AdminUsecase)

	j, err := json.Marshal(tempMockAdmin)
	assert.NoError(t, err)
	stubToken := &usecase.JwtToken{
		Token: "test token",
	}
	mockUcase.On("GetTokenByNameAndPass", mock.AnythingOfType("string"), mock.AnythingOfType("string")).Return(stubToken, nil)
	e := echo.New()
	req, err := http.NewRequest(echo.POST, "/login", strings.NewReader(string(j)))
	assert.NoError(t, err)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/login")

	log := logrus.New()
	handler := httpAdmin.HttpAdminHandler{
		ADMINusecase:mockUcase, LOG:log,
	}


	handler.GetTokenByNameAndPass(c)

	assert.Equal(t, http.StatusOK, rec.Code)
}

func TestHttpAdminHandler_GetNameByToken(t *testing.T) {
	mockAdmin := models.Admin{
		Name:     "Emperor",
	}

	mockUcase := new(mocks.AdminUsecase)
	mockUcase.On("GetNameByToken", mock.AnythingOfType("string")).Return(&mockAdmin, nil)
	e := echo.New()
	req, err := http.NewRequest(echo.GET, "/login", strings.NewReader(""))
	assert.NoError(t, err)
	req.Header.Set("authorization", "Bearer test token")

	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/login")

	log := logrus.New()
	handler := httpAdmin.HttpAdminHandler{
		  ADMINusecase:mockUcase, LOG:log,
	}

	handler.GetNameByToken(c)

	assert.Equal(t, http.StatusOK, rec.Code)
	target := `{"id":0,"name":"` + mockAdmin.Name +`","password":""}`
	assert.Equal(t, target, rec.Body.String())
}
