package admin_nw

import (
	"github.com/labstack/echo"
	"net/http"
	"github.com/sirupsen/logrus"
	"gopkg.in/go-playground/validator.v9"
	models "trial-mito/admin"
	adminUcase "trial-mito/admin/usecase"
)

//HttpAdminHandler struct defines the handler
//by its usecase, logger instance and a Jwt middleware.
type HttpAdminHandler struct {
	ADMINusecase adminUcase.AdminUsecase
	LOG *logrus.Logger
	JwtMiddleware echo.MiddlewareFunc
}

//NewContactHttpHandler() sets the handler instance and paths.
func NewAdminHttpHandler(e *echo.Echo, ADMINusecase adminUcase.AdminUsecase,  LOG *logrus.Logger, JwtMiddleware echo.MiddlewareFunc){
	handler := &HttpAdminHandler{
		ADMINusecase, LOG, JwtMiddleware,
	}
	e.GET("/login", handler.GetNameByToken, JwtMiddleware)
	e.POST("/login", handler.GetTokenByNameAndPass)
}

//GetTokenByNameAndPass() validates the received model, checks if
//an admin record with the same name and password exists in the database
//and if yes returns a newly created token in json format.
func (ad *HttpAdminHandler) GetTokenByNameAndPass(c echo.Context) error {
	ad.LOG.Debug("HttpAdminHandler::GetTokenByNameAndPass()")
	var admin models.Admin
	err := c.Bind(&admin)
	if err != nil {
		return c.JSON(http.StatusUnprocessableEntity, err.Error())
	}

	if ok, err := ad.isRequestValid(&admin); !ok {
		ad.LOG.Warning("a bad request happened!")
		return c.JSON(http.StatusBadRequest, err.Error())
	}
	t, err := ad.ADMINusecase.GetTokenByNameAndPass(admin.Name, admin.Password)
	if err != nil {
		return c.JSON(ad.getStatusCode(err), err.Error())
	}
	ad.LOG.Info("a token successfully created")
	return c.JSON(http.StatusOK, t)
}

//GetNameByToken() decrypt the received token then returns the name from it.
func(ad *HttpAdminHandler) GetNameByToken(c echo.Context) error {
	ad.LOG.Debug("HttpAdminHandler::GetNameByToken()")
	t := c.Request().Header.Get("authorization")
	adm, err := ad.ADMINusecase.GetNameByToken(t)
	if err != nil {
		return c.JSON(ad.getStatusCode(err), err.Error())
	}
	ad.LOG.Info(adm.Name," on the board!")
	return c.JSON(http.StatusOK, adm)
}


//isRequestValid() validates the received model from the request.
func (ad *HttpAdminHandler)isRequestValid(m *models.Admin) (bool, error) {
	ad.LOG.Debug("HttpAdminHandler::isRequestValid()")
	validate := validator.New()
	err := validate.Struct(m)
	if err != nil {
		return false, err
	}
	return true, nil
}

//getStatuscode() uniformise error handling and returns custom
//error messages.
func (ad *HttpAdminHandler)getStatusCode(err error) int {
	ad.LOG.Debug("HttpAdminHandler::getStatusCode()")
	if err == nil {
		return http.StatusOK
	}

	logrus.Error(err)
	switch err {
	case models.NOT_FOUND_ERROR:
		return http.StatusNotFound
	default:
		return http.StatusInternalServerError
	}
}