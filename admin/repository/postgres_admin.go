package repository

import (
	models "trial-mito/admin"
	"database/sql"
	"github.com/sirupsen/logrus"
)

 type postgresAdminRepository struct {
 	Conn *sql.DB
 	LOG *logrus.Logger
 }

//NewPostgresAdminRepository() returns an AdminRepository instance
//with its database driver and logger instances.
func NewPostgresAdminRepository(Conn *sql.DB, 	LOG *logrus.Logger) AdminRepository {
	return &postgresAdminRepository{Conn,LOG}
}

//GetByNameAndPass()makes the query to select one record from the database and sends it
//to the fetch() logic. Returns a contact model.
func (p *postgresAdminRepository) GetByNameAndPass(n string, ps string) (*models.Admin, error) {
	p.LOG.Debug("postgresAdminRepository::GetByNameAndPass()")
	query := `SELECT name, password FROM administrators WHERE name=$1 AND password = $2`

	list, err := p.fetch(query, n, ps)
	if err != nil {
		return nil, err
	}

	a := &models.Admin{}
	if len(list) > 0 {
		a = list[0]
	} else {
		return nil, models.NOT_FOUND_ERROR
	}
	return a, nil
}

//fetch() uniformize queries, fill them with args and return result contact models
//collected into a slice. It exists cause of future improvements when more
//logic will use it.
func (p *postgresAdminRepository) fetch(query string, args ...interface{}) ([]*models.Admin, error) {
	p.LOG.Debug("postgresAdminRepository::fetch()")
	rows, err := p.Conn.Query(query, args...)
	if err != nil {
		p.LOG.Error(err)
		return nil, err
	}
	defer rows.Close()

	result := make([]*models.Admin, 0)
	for rows.Next() {
		t := new(models.Admin)
		err = rows.Scan(
			//&t.ID,
			&t.Name,
			&t.Password,
		)
		if err != nil {
			p.LOG.Error(err)
			return nil, err
		}
		result = append(result, t)
	}
	return result, nil
}