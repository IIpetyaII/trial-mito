package repository_test

import (
	"testing"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	"github.com/stretchr/testify/assert"
	adminRepo "trial-mito/admin/repository"
	"github.com/sirupsen/logrus"
)

func TestGetByNameAndPass(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	rows := sqlmock.NewRows([]string{"name","password"}).
		AddRow("Turbók Lajos", "abc123")
	query := `[SELECT name, password FROM administrators WHERE name=$1 AND password=$2]`
	mock.ExpectQuery(query).WillReturnRows(rows)

	log := logrus.New()
	ar := adminRepo.NewPostgresAdminRepository(db, log)

	name := "Turbók Lajos2"
	password:= "abc123password"
	ok, err := ar.GetByNameAndPass(name,password)
	assert.NoError(t, err)
	assert.NotNil(t, ok)
}
