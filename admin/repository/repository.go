package repository

import models "trial-mito/admin"

//AdminRepository defines the behaviour of a repository implementation
type AdminRepository interface {
	GetByNameAndPass(n string, p string) (*models.Admin, error)
}