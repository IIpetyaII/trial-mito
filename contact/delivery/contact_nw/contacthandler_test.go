package contact_nw_test

import (
	"testing"
	models "trial-mito/contact"
	"github.com/bxcodec/faker"
	"github.com/stretchr/testify/assert"
	"trial-mito/contact/usecase/mocks"
	"github.com/labstack/echo"
	"net/http"
	"strings"
	"net/http/httptest"
	contactHttp "trial-mito/contact/delivery/contact_nw"
	"strconv"
	"encoding/json"
	"github.com/stretchr/testify/mock"
	"github.com/sirupsen/logrus"
)

func TestHttpContactHandler_FetchContact(t *testing.T) {
	var mockContact models.Contact
	err:= faker.FakeData(&mockContact)
	assert.NoError(t, err)
	mockUCase := new(mocks.ContactUsecase)
	mockListContact := make([]*models.Contact,0)
	mockListContact = append(mockListContact, &mockContact)
	num:=0
	cursor:=1
	mockUCase.On("Fetch",int64(num),int64(cursor)).Return(mockListContact,"10",nil)

	e := echo.New()
	req,err := http.NewRequest(echo.GET,"/contacts?num=0&cursor=1", strings.NewReader(""))
	assert.NoError(t, err)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	log := logrus.New()
	handler := contactHttp.HttpContactHandler{
		mockUCase, log,
	}
	handler.FetchContact(c)

	responseCursor := rec.Header().Get("X-Cursor")
	assert.Equal(t,"10",responseCursor)

	assert.Equal(t, http.StatusOK,rec.Code)
	mockUCase.AssertExpectations(t)
}

func TestHttpContactHandler_FetchContactError(t *testing.T) {
	mockUCase := new(mocks.ContactUsecase)
	num:=0
	cursor:=1
	mockUCase.On("Fetch",int64(num),int64(cursor)).Return(nil,"",models.INTERNAL_SERVER_ERROR)

	e := echo.New()
	req,err := http.NewRequest(echo.GET,"/contacts?num=0&cursor=1", strings.NewReader(""))
	assert.NoError(t, err)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	log := logrus.New()
	handler := contactHttp.HttpContactHandler{
		mockUCase,	log,

	}
	handler.FetchContact(c)

	responseCursor := rec.Header().Get("X-Cursor")
	assert.Equal(t,"",responseCursor)

	assert.Equal(t, http.StatusInternalServerError, rec.Code)
	mockUCase.AssertExpectations(t)
}

func TestHttpContactHandler_GetByID(t *testing.T) {
	var mockContact models.Contact
	err:= faker.FakeData(&mockContact)
	assert.NoError(t, err)
	mockUCase := new(mocks.ContactUsecase)
	num := int(mockContact.ID)

	mockUCase.On("GetByID", int64(num)).Return(&mockContact,nil)

	e := echo.New()
	req,err := http.NewRequest(echo.GET, "/contact/" +strconv.Itoa(int(num)), strings.NewReader(""))
	assert.NoError(t,err)

	rec:= httptest.NewRecorder()
	c := e.NewContext(req,rec)

	c.SetPath("/contact/:id")
	c.SetParamNames("id")
	c.SetParamValues(strconv.Itoa(num))
	log := logrus.New()
	handler := contactHttp.HttpContactHandler{
		CNTCTusecase: mockUCase, LOG: log,
	}

	handler.GetByID(c)

	assert.Equal(t,http.StatusOK,rec.Code)
	mockUCase.AssertExpectations(t)
}

func TestHttpContactHandler_Store(t *testing.T) {
	mockContact := models.Contact{
		Name:"Max Power",
		Email: "mp@email.com",
		Phone: "32423423433",
	}

	tempMockContact := mockContact
	tempMockContact.ID = 0

	mockUcase := new(mocks.ContactUsecase)

	j, err := json.Marshal(tempMockContact)
	assert.NoError(t,err)

	mockUcase.On("Store", mock.AnythingOfType("*contact.Contact")).Return(int64(1),nil)

	e := echo.New()
	req, err := http.NewRequest(echo.POST, "/contact", strings.NewReader(string(j)))
	assert.NoError(t, err)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)

	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/contact")

	log := logrus.New()
	handler := contactHttp.HttpContactHandler{
		CNTCTusecase: mockUcase, LOG: log,
	}

	handler.Store(c)

	assert.Equal(t,http.StatusCreated, rec.Code)
	mockUcase.AssertExpectations(t)
}

func TestHttpContactHandler_Update(t *testing.T) {
	mockContact := models.Contact{
		Name:"Max Power",
		Email: "mp@email.com",
		Phone: "32423423433",
	}

	tempMockContact := mockContact
	tempMockContact.ID = 0

	mockUcase := new(mocks.ContactUsecase)
	num := int(mockContact.ID)

	j, err := json.Marshal(tempMockContact)
	assert.NoError(t,err)

	mockUcase.On("Update", mock.AnythingOfType("*contact.Contact")).Return(true,nil)

	e := echo.New()
	req, err := http.NewRequest(echo.PUT, "/contact/"+strconv.Itoa(int(num)), strings.NewReader(string(j)))
	assert.NoError(t, err)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)

	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/contact/:id")

	log := logrus.New()
	handler := contactHttp.HttpContactHandler{
		CNTCTusecase: mockUcase, LOG: log,
	}

	handler.Update(c)

	assert.Equal(t,http.StatusAccepted, rec.Code)
	mockUcase.AssertExpectations(t)
}

func TestHttpContactHandler_Delete(t *testing.T) {
	var mockContact models.Contact
	err:= faker.FakeData(&mockContact)
	assert.NoError(t, err)
	mockUCase := new(mocks.ContactUsecase)
	num := int(mockContact.ID)

	mockUCase.On("Delete", int64(num)).Return(true, nil)

	e := echo.New()
	req, err := http.NewRequest(echo.DELETE, "/contact/"+strconv.Itoa(int(num)), strings.NewReader(""))
	assert.NoError(t, err)

	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("contact/:id")
	c.SetParamNames("id")
	c.SetParamValues(strconv.Itoa(num))

	log := logrus.New()
	handler := contactHttp.HttpContactHandler{
		CNTCTusecase: mockUCase, LOG: log,
	}

	handler.Delete(c)

	assert.Equal(t, http.StatusOK, rec.Code)
	mockUCase.AssertExpectations(t)
}

