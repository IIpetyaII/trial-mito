package contact_nw

import (
	models "trial-mito/contact"
	"github.com/labstack/echo"
	"strconv"
	"net/http"
	contactUcase "trial-mito/contact/usecase"

	"github.com/sirupsen/logrus"
	"gopkg.in/go-playground/validator.v9"
)

//Uniformized json message struct.
type Message struct{
	Message string `json:"message"`
	ID string `json:"id"`
}

//HttpContactHandler struct defines the handler
//by its usecase, and logger instance.
type HttpContactHandler struct {
	CNTCTusecase contactUcase.ContactUsecase
	LOG *logrus.Logger
}

//NewContactHttpHandler() sets the handler instance and paths.
func NewContactHttpHandler(e *echo.Echo, CNTCTusecase contactUcase.ContactUsecase, LOG *logrus.Logger) {
	handler := &HttpContactHandler{
		CNTCTusecase, LOG,
	}

	e.GET("/contacts", handler.FetchContact)
	e.POST("/contacts", handler.Store)
	e.GET("/contacts/:id", handler.GetByID)
	e.DELETE("/contacts/:id", handler.Delete)
	e.PUT("/contacts/:id", handler.Update)
}

//GetByID() returns a contact model by its id.
func (co *HttpContactHandler) GetByID(c echo.Context) error {
	co.LOG.Debug("HttpContactHandler::GetByID()")
	idP, err := strconv.Atoi(c.Param("id"))
	id := int64(idP)

	con, err := co.CNTCTusecase.GetByID(id)

	if err != nil {
		return c.JSON(co.getStatusCode(err), err.Error())
	}
	return c.JSON(http.StatusOK, con)
}

//Delete() check for id param in the Context, sends the id
//to the handler for deleting a record, then send back a message with
//the id of the deleted row.
func (co *HttpContactHandler) Delete(c echo.Context) error {
	co.LOG.Debug("HttpContactHandler::Delete()")
	idP, err := strconv.Atoi(c.Param("id"))
	id := int64(idP)

	_, err = co.CNTCTusecase.Delete(id)
	if err != nil {
		return c.JSON(co.getStatusCode(err), err.Error())
	}
	co.LOG.Info("a contact successfully deleted")
	return c.JSON(http.StatusOK,&Message{Message:"contact deleted"})
}

//Store validates the received model before send it to persistent logic.
//Returns a message
func (co *HttpContactHandler) Store(c echo.Context) error {
	co.LOG.Debug("HttpContactHandler::Store()")
	var contact models.Contact
	err := c.Bind(&contact)
	if err != nil {
		return c.JSON(http.StatusUnprocessableEntity, err.Error())
	}

	if ok, err := co.isRequestValid(&contact); !ok {
		co.LOG.Warning("a bad Store request happened!")
		return c.JSON(http.StatusBadRequest, err.Error())
	}

	id, err := co.CNTCTusecase.Store(&contact)
	if err != nil {
		return c.JSON(co.getStatusCode(err), err.Error())
	}

	newId := strconv.FormatInt(id,10)
	message := `New contact created with id:` +  newId
	co.LOG.Info("a new contact successfully stored")
	return c.JSON(http.StatusCreated,&Message{Message: message, ID: newId})
}

//Update() updates a contact by its id. At the end of the process it returns
//a 202 admin_nw status code, with a message.
func (co *HttpContactHandler) Update(c echo.Context) error {
	co.LOG.Debug("HttpContactHandler::Update()")
	idP, err := strconv.Atoi(c.Param("id"))
	id := int64(idP)

	var contact models.Contact
	contact.ID = id
	err = c.Bind(&contact)
	if err != nil {
		return c.JSON(http.StatusUnprocessableEntity, err.Error())
	}

	if ok, err := co.isRequestValid(&contact); !ok {
		co.LOG.Warning("a bad request happened!")
		return c.JSON(http.StatusBadRequest, err.Error())
	}

	_, err = co.CNTCTusecase.Update(&contact)
	if err != nil {
		return c.JSON(co.getStatusCode(err), err.Error())
	}
	co.LOG.Info("a contact successfully updated")
	return c.JSON(http.StatusAccepted, &Message{Message:"contact updated"} )
}

//FetchContact() fetches a batch of contacts. The received cursor params,
//defines from which record you want the load to start, and num param defines the
//number of records. In default, when this two is zero, the CNTCTusecase.Fetch() defines
//default number for num param. When the load returns from CNTCTusecase.Fetch(), FetchContact()
//sets a custom Header key X-Cursor with a value with the id of the last returned contact model.
//By the X-Cursor param the client can set the next requests cursor param. Possible future improvement
//is database with string cursors.
func (co *HttpContactHandler) FetchContact(c echo.Context) error {
	co.LOG.Debug("HttpContactHandler::FetchContact()")
	numS := c.QueryParam("num")
	num, _ := strconv.Atoi(numS)
	cursorS := c.QueryParam("cursor")
	cursor, _ := strconv.Atoi(cursorS)


	listCon, nextCursor, err := co.CNTCTusecase.Fetch(int64(num), int64(cursor))
	if err != nil {
		return c.JSON(co.getStatusCode(err), err.Error())
	}

	c.Response().Header().Set(`X-Cursor`, nextCursor)
	return c.JSON(http.StatusOK, listCon)
}

//isRequestValid() validates the received model from the request.
func (co *HttpContactHandler)isRequestValid(m *models.Contact) (bool, error) {
	validate := validator.New()

	err := validate.Struct(m)
	if err != nil {
		return false, err
	}

	return true, nil
}

//getStatuscode() uniformise error handling and returns custom
//error messages.
func (co *HttpContactHandler)getStatusCode(err error) int {
	if err == nil {
		return http.StatusOK
	}

	logrus.Error(err)
	switch err {
	case models.INTERNAL_SERVER_ERROR:
		return http.StatusInternalServerError
	case models.NOT_FOUND_ERROR:
		return http.StatusNotFound
	case models.CONFLICT_ERROR:
		return http.StatusConflict
	default:
		return http.StatusInternalServerError
	}
}
