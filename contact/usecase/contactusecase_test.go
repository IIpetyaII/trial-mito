package usecase_test

import (
	"trial-mito/contact/repository/mocks"
	models "trial-mito/contact"
	"github.com/stretchr/testify/mock"
	"testing"
	ucase "trial-mito/contact/usecase"
	"github.com/stretchr/testify/assert"
	"strconv"
	"github.com/sirupsen/logrus"
)

func TestFetch(t *testing.T) {

	mockContactRepo := new(mocks.ContactRepository)
	mockContact := &models.Contact{
		ID:    3,
		Name:  `Májkel Nájt`,
		Email: `majkel@kitt.hu`,
		Phone: "3423234234",
	}

	mockListContact := make([]*models.Contact, 0)
	mockListContact = append(mockListContact, mockContact)
	mockContactRepo.On("Fetch", mock.AnythingOfType("int64"), mock.AnythingOfType("int64")).Return(mockListContact, nil)

	log := logrus.New()
	u := ucase.NewContactUsecase(mockContactRepo, log)
	num := int64(1)
	cursor := int64(10)
	list, nextCursor, err := u.Fetch(num, cursor)
	cursorExpected := strconv.FormatInt(mockContact.ID, 10)
	assert.Equal(t, cursorExpected, nextCursor)
	assert.NotEmpty(t, nextCursor)
	assert.NoError(t, err)
	assert.Len(t, list, len(mockListContact))

	mockContactRepo.AssertCalled(t, "Fetch", mock.AnythingOfType("int64"), mock.AnythingOfType("int64"))

}

func TestGetByID(t *testing.T) {
	mockContactRepo := new(mocks.ContactRepository)
	mockContact := models.Contact{
		ID:    3,
		Name:  `Májkel Nájt`,
		Email: `majkel@kitt.hu`,
		Phone: "3423234234",
	}

	mockContactRepo.On("GetByID", mock.AnythingOfType("int64")).Return(&mockContact, nil)
	defer mockContactRepo.AssertCalled(t, "GetByID", mock.AnythingOfType("int64"))
	log := logrus.New()
	u := ucase.NewContactUsecase(mockContactRepo, log)
	c, err := u.GetByID(mockContact.ID)
	assert.NoError(t, err)
	assert.NotNil(t, c)
}

func TestStore(t *testing.T) {
	mockContactRepo := new(mocks.ContactRepository)
	mockContact := models.Contact{
		Name:  `Májkel Nájt`,
		Email: `majkel@kitt.hu`,
		Phone: "3423234234",
	}
	//set to 0 because this is test from Client, and ID is an AutoIncreament
	tempMockContact := mockContact
	tempMockContact.ID = 0

	mockContactRepo.On("GetByName", mock.AnythingOfType("string")).Return(nil, models.NOT_FOUND_ERROR)
	mockContactRepo.On("Store", mock.AnythingOfType("*contact.Contact")).Return(mockContact.ID, nil)
	defer mockContactRepo.AssertCalled(t, "GetByName", mock.AnythingOfType("string"))
	defer mockContactRepo.AssertCalled(t, "Store", mock.AnythingOfType("*contact.Contact"))
	log := logrus.New()
	u := ucase.NewContactUsecase(mockContactRepo, log)

	c, err := u.Store(&tempMockContact)

	assert.NoError(t, err)
	assert.NotNil(t, c)
	assert.Equal(t, mockContact.Name, tempMockContact.Name)
}

func TestDelete(t *testing.T) {
	log := logrus.New()
	mockContactRepo := new(mocks.ContactRepository)
	mockContact := models.Contact{
		Name:  `Májkel Nájt`,
		Email: `majkel@kitt.hu`,
		Phone: "3423234234",
	}

	mockContactRepo.On("GetByID", mock.AnythingOfType("int64")).Return(&mockContact, nil)
	defer mockContactRepo.AssertCalled(t, "GetByID", mock.AnythingOfType("int64"))

	mockContactRepo.On("Delete", mock.AnythingOfType("int64")).Return(true, nil)
	defer mockContactRepo.AssertCalled(t, "Delete", mock.AnythingOfType("int64"))

	u := ucase.NewContactUsecase(mockContactRepo, log)
	b, err := u.Delete(mockContact.ID)

	assert.NoError(t, err)
	assert.True(t, b)
}

func TestUpdate(t *testing.T){
	log := logrus.New()
	mockContactRepo := new(mocks.ContactRepository)
	mockContact := models.Contact{
		Name:  `Májkel Nájt`,
		Email: `majkel@kitt.hu`,
		Phone: "3423234234",
	}

	mockContactRepo.On("Update", mock.AnythingOfType("*contact.Contact")).Return(true, nil)
	defer mockContactRepo.AssertCalled(t, "Update", mock.AnythingOfType("*contact.Contact"))

	u := ucase.NewContactUsecase(mockContactRepo,log)

	b, err := u.Update(&mockContact)

	assert.NoError(t, err)
	assert.True(t, b)
}