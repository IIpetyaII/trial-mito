package usecase

import (
	"trial-mito/contact"
	"trial-mito/contact/repository"
	"strconv"
	"github.com/sirupsen/logrus"
)

//ContactUsecase interface defines the behavior a usecase
//implementation.
type ContactUsecase interface {
	Fetch(num int64, cursor int64) ([]*contact.Contact, string, error)
	GetByID(id int64) (*contact.Contact, error)
	GetByName(title string) (*contact.Contact, error)
	Update(ar *contact.Contact) (bool, error)
	Store(*contact.Contact) (int64, error)
	Delete(id int64) (bool, error)
}

//contactUsecase struct defines the usecase by its
//repository and logger instance.
type contactUsecase struct {
	contactRepos repository.ContactRepository
	LOG *logrus.Logger
}

//NewContactUseCase() returns a contactUsecase instance
//with its repository and logger instances.
func NewContactUsecase(contactRepos repository.ContactRepository,LOG *logrus.Logger) ContactUsecase{
	return &contactUsecase{
		contactRepos, LOG,
	}
}

//Fech() defines the max load of batch of contact models(save the server and client
//from a too big chunk of contact models in memory) by the num param.
//If num is zero, it sets a default value. When the load returns it checks
//for the index of the last record and send it to the handler(to send
//it to the client in the X-Cursor key.)
func (c *contactUsecase) Fetch(num int64, cursor int64 ) ([]*contact.Contact, string, error) {
	c.LOG.Debug("ContactUsecase::Fetch()")
	if num == 0 {
		num = 200
	}

	listContact, err := c.contactRepos.Fetch(num, cursor)
	if err != nil {
		return nil, "", err
	}

	nextCursor := ""

	//X-Cursor field in the header generated only in this case
	if size := len(listContact); size == int(num) {
		lastId := listContact[num-1].ID
		nextCursor = strconv.Itoa(int(lastId))
	}
	return listContact, nextCursor, nil
}

//GetByID() sends id to repository handler, the returns the received contact model.
func (c *contactUsecase) GetByID(id int64) (*contact.Contact, error) {
	c.LOG.Debug("ContactUsecase::GetByID()")
	res, err := c.contactRepos.GetByID(id)
	if err != nil {
		return nil, err
	}
	return res, nil
}

//Update() returns a true if repository hadler Update()
//executed correctly.
func (c *contactUsecase) Update(co *contact.Contact) (bool, error) {
	c.LOG.Debug("ContactUsecase::Update()")
	return c.contactRepos.Update(co)
}

//Store() checks if the received model exists in the database
//by its name. If it's not send it to persistent logic, then returns
//the newly created records id.
func (c *contactUsecase) Store(co *contact.Contact) (int64, error) {
	c.LOG.Debug("ContactUsecase::Store()")
	existedContact, _ := c.contactRepos.GetByName(co.Name)
	if existedContact != nil {
		return 0, contact.CONFLICT_ERROR
	}

	return c.contactRepos.Store(co)
}

//GetByName() returns a contact model by its name.
func (c *contactUsecase) GetByName(nm string) (*contact.Contact, error) {
	c.LOG.Debug("*ContactUsecase::GetByName()")
	res, err := c.contactRepos.GetByName(nm)
	if err != nil {
		return nil, err
	}
	return res, nil
}

//Delete() checks if the model wanted to delete exists, if yes
//it sends it repository handler's delete logic, and if executed
//successfully returns a true bool.
func (c *contactUsecase) Delete(id int64) (bool, error) {
	c.LOG.Debug("*ContactUsecase::Delete()")
	existedContact, _ := c.contactRepos.GetByID(id)
	if existedContact == nil {
		return false, contact.NOT_FOUND_ERROR
	}
	return c.contactRepos.Delete(id)
}