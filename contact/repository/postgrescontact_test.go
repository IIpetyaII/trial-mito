package repository_test

import (
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	"testing"
	contactRepo "trial-mito/contact/repository"
	"github.com/stretchr/testify/assert"
	models "trial-mito/contact"
	"github.com/sirupsen/logrus"
)

func TestFetch(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}

	defer db.Close()
	rows := sqlmock.NewRows([]string{"id","name","email","phone"}).
		AddRow(1,"Öcsi Pöcsi", "öpcsi@email.com", "18181818").
		AddRow(1,"Margit", "sunshine69@email.com", "1854541818")

	query := `[SELECT id, name, email, phone FROM contacts WHERE id > $1 LIMIT $2]`

	mock.ExpectQuery(query).WillReturnRows(rows)

	log := logrus.New()
	c := contactRepo.NewPostgresContactRepository(db,log)

	cursor := int64(1)
	num := int64(5)
	list, err := c.Fetch(cursor, num)
	assert.NoError(t, err)
	assert.Len(t, list, 2)

}

func TestGetByID(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	rows := sqlmock.NewRows([]string{"id","name","email","phone"}).
		AddRow(1,"Öcsi Pöcsi", "öpcsi@email.com", "18181818")

	query := "[SELECT id, name, email, phone FROM contacts WHERE id=$1]"

	mock.ExpectQuery(query).WillReturnRows(rows)
	log := logrus.New()
	c := contactRepo.NewPostgresContactRepository(db,log)

	num := int64(5)
	contact, err := c.GetByID(num)
	assert.NoError(t, err)
	assert.NotNil(t, contact)
}

func TestGetByName(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	rows := sqlmock.NewRows([]string{"id","name","email","phone"}).
		AddRow(1,"Öcsi Pöcsi", "öpcsi@email.com", "18181818")

	query := "[`SELECT name, email, phone FROM contacts WHERE name=$1`]"

	mock.ExpectQuery(query).WillReturnRows(rows)
	log := logrus.New()
	c := contactRepo.NewPostgresContactRepository(db,log)

	name := "Öcsi Pöcsi"
	con, err := c.GetByName(name)
	assert.NoError(t, err)
	assert.NotNil(t, con)
}

func TestDelete(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	query := `[DELETE FROM contacts WHERE id=$1]`

	prep := mock.ExpectPrepare(query)
	prep.ExpectExec().WithArgs(12).WillReturnResult(sqlmock.NewResult(12, 1))
	log := logrus.New()
	a := contactRepo.NewPostgresContactRepository(db, log)

	num := int64(12)
	contactStatus, err := a.Delete(num)
	assert.NoError(t, err)
	assert.True(t, contactStatus)
}


func TestUpdate(t *testing.T) {
	c := &models.Contact{
		ID:	1,
		Name:   "Karajos László",
		Email: "karajlaci@gmail.com",
		Phone: "1212112111",
	}

	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	query := `[UPDATE contacts SET name=$1, email=$2, phone=$3 WHERE id=$4]`

	prep := mock.ExpectPrepare(query)
	prep.ExpectExec().WithArgs(c.Name, c.Email, c.Phone, c.ID).WillReturnResult(sqlmock.NewResult(45, 1))

	log := logrus.New()
	cr := contactRepo.NewPostgresContactRepository(db,log)

	b, err := cr.Update(c)
	assert.NoError(t, err)
	assert.NotNil(t, b)
	assert.Equal(t,true,b)
}

func TestStore(t *testing.T) {
	c := &models.Contact{
		ID: 1,
		Name:   "Öcsi Pöcsi",
		Email: "öpcsi@email.com",
		Phone: "18181818",
	}
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	row := sqlmock.NewRows([]string{"id"}).
		AddRow(1)

	query := "[INSERT INTO contacts(name, email, phone) VALUES($1, $2, $3) RETURNING id]"
	prep := mock.ExpectPrepare(query)
	prep.ExpectQuery().WithArgs(c.Name, c.Email, c.Phone).WillReturnRows(row)
	log := logrus.New()
	cr := contactRepo.NewPostgresContactRepository(db, log)

	id, err := cr.Store(c)
	assert.NoError(t, err)
	assert.Equal(t, c.ID, id)
}
