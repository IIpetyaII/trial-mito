package repository

import models "trial-mito/contact"

//ContactRepository defines the behaviour of a repository implementation
type ContactRepository interface {
	Fetch( num int64, cursor int64) ([]*models.Contact, error)
	GetByID(id int64) (*models.Contact, error)
	GetByName(n string) (*models.Contact, error)
	Update(c *models.Contact) (bool, error)
	Store(c *models.Contact) (int64, error)
	Delete(id int64) (bool, error)
}