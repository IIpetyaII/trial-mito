package repository

import (
	"database/sql"
	"github.com/sirupsen/logrus"
	models "trial-mito/contact"
	"fmt"
)

//postgresContactRepository struct defines the repository handler by its
//database driver and a logger instance.
type postgresContactRepository struct {
	Conn *sql.DB
	LOG *logrus.Logger
}

//NewPostgresContactRepository() returns a ContactRepository instance
//with its database driver and logger instances.
func NewPostgresContactRepository(Conn *sql.DB, LOG *logrus.Logger) ContactRepository {
	return &postgresContactRepository{Conn,LOG}
}

//fetch() uniformize queries, fill them with args and return result contact models
//collected into a slice.
func (p *postgresContactRepository) fetch(query string, args ...interface{}) ([]*models.Contact, error) {
	p.LOG.Debug("postgresContactRepository::fetch()")
	rows, err := p.Conn.Query(query, args...)

	if err != nil {
		logrus.Error(err)
		return nil, err
	}
	defer rows.Close()
	result := make([]*models.Contact, 0)
	for rows.Next() {
		t := new(models.Contact)
		err = rows.Scan(
			&t.ID,
			&t.Name,
			&t.Email,
			&t.Phone,
		)
		if err != nil {
			logrus.Error(err)
			return nil, err
		}
		result = append(result, t)
	}
	return result, nil
}

//Fetch() make the SELECT query for a batch of contact models then send to fetch() logic.
//Returns the slice of the result contact models.
func (p *postgresContactRepository) Fetch(num int64, cursor int64) ([]*models.Contact, error) {
	p.LOG.Debug("postgresContactRepository::Fetch()")
	query := `SELECT id, name, email, phone FROM contacts ORDER  BY id ASC LIMIT $1 OFFSET $2`
	return p.fetch(query, num , cursor)

}

//GetByID() makes the query to select one record from the database and sends it
//to the fetch() logic. Returns a contact model.
func (p *postgresContactRepository) GetByID(id int64) (*models.Contact, error) {
	p.LOG.Debug("postgresContactRepository::GetByID()")

	query := `SELECT id, name, email, phone FROM contacts WHERE id=$1`

	list, err := p.fetch(query, id)
	if err != nil {
		return nil, err
	}

	a := &models.Contact{}
	if len(list) > 0 {
		a = list[0]
	} else {
		return nil, models.NOT_FOUND_ERROR
	}

	return a, nil
}

//GetByName() makes the query to select one record from the database and sends it
//to the fetch() logic. Returns a contact model if it exists.
func (p *postgresContactRepository) GetByName(n string) (*models.Contact, error) {
	p.LOG.Debug("postgresContactRepository::GetByName()")

	query := `SELECT name, email, phone FROM contacts WHERE name=$1`

	list, err := p.fetch(query, n)
	if err != nil {
		return nil, err
	}

	a := &models.Contact{}
	if len(list) > 0 {
		a = list[0]
	} else {
		return nil, models.NOT_FOUND_ERROR
	}
	return a, nil
}

//Store(c) insert a new a record to the database then return the id of the new row.
//It uses prepared statement against SQL injections.
func (p *postgresContactRepository) Store(c *models.Contact) (int64, error) {
	p.LOG.Debug("postgresContactRepository::Store()")
	query := `INSERT INTO contacts(name, email, phone) VALUES($1, $2, $3) RETURNING id`
	stmt, err := p.Conn.Prepare(query)
	if err != nil {
		return 0, err
	}
	err = stmt.QueryRow(c.Name, c.Email, c.Phone).Scan(&c.ID)
	if err != nil {
		return 0, err
	}
	return c.ID, nil
}

//Delete() deletes a record from the database then return true bool
//if the process executed without problems. Checks for affected rows too!
//It uses prepared statement against SQL injections.
func (p *postgresContactRepository) Delete(id int64) (bool, error) {
	p.LOG.Debug("postgresContactRepository::Store()")
	query := `DELETE FROM contacts WHERE id=$1`

	stmt, err := p.Conn.Prepare(query)
	if err != nil {
		return false, err
	}
	res, err := stmt.Exec(id)
	if err != nil {
		return false, err
	}
	rowsAfected, err := res.RowsAffected()
	if err != nil {
		return false, err
	}
	if rowsAfected != 1 {
		err = fmt.Errorf("Weird  Behaviour. Total Affected: %d", rowsAfected)
		logrus.Error(err)
		return false, err
	}

	return true, nil
}

//Update() updates a record in the database then return true bool
//if the process executed without problems. Checks for affected rows too!
//It uses prepared statement against SQL injections.
func (p *postgresContactRepository) Update(c *models.Contact) (bool, error) {
	p.LOG.Debug("postgresContactRepository::Update()")
	query := `UPDATE contacts SET name=$1, email=$2, phone=$3 WHERE id=$4`

	stmt, err := p.Conn.Prepare(query)
	if err != nil {
		return false, nil
	}

	res, err := stmt.Exec(c.Name, c.Email, c.Phone, c.ID)
	if err != nil {
		return false, err
	}
	affect, err := res.RowsAffected()
	if err != nil {
		return false, err
	}
	if affect != 1 {
		err = fmt.Errorf("Weird  Behaviour. Total Affected: %d", affect)
		logrus.Error(err)
		return false, err
	}

	return true, nil
}
